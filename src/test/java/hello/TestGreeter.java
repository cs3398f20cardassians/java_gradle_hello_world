package hello;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

// Change the comment again to kick off a build
// Julian's comment

public class TestGreeter 
{

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty()
   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Name='Planet'")
   public void testGreeter()
   {
      g.setName("Planet");
      assertEquals(g.getName(),"Planet");
      assertEquals(g.sayHello(),"Hello Planet!");
   }
   
   @Test
   @DisplayName("Test for Name='Julian'")
   public void testPersonalGreeter()
   {
      g.setName("Julian");
      assertEquals(g.getName(),"Julian");
      assertEquals(g.sayHello(),"Hello Julian!");
   }

   @Test
   @DisplayName("Test for incorrect name")
   public void testIncorrectGreeter()
   {
	   g.setName("NotJulian");
	   assertFalse(g.getName().equals("Julian"));
   }
}
